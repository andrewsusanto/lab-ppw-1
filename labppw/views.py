from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    name='Andrew'
    context={
        'name': name,
        'npm':1906350692
    }
    return render(request,"index.html",context)
